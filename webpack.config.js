"use strict";
var settings = require("./src/settings.json"),
    webpack = require("webpack"),
    path = require("path"),
    publicPath = settings.live ? settings.livePath : settings.devPath,
    fs = require("fs"),
    filename = "live.js",
    templatePlugin = new webpack.DefinePlugin({
        "__RETAILER__": JSON.stringify(settings.retailer),
        "__GUTTERS__": JSON.stringify(settings.gutters_template),
        "__CONTAINERBD__": JSON.stringify(settings.color_container),
        "__PLACEHOLDER__": JSON.stringify("storetail-id-" + settings.format + settings.creaid),
        "__REALPATH__": publicPath
    }),
    snippet = `__sto.deliv({"placements":{"${settings.format}": {"script": "${publicPath}${filename}"}}})`;
    console.log(templatePlugin);

// création d'un snippet dans dist/snippet
// le snippet permet de simuler un appel à l'adserver pour pouvoir tester le format sans
// créer de campagne

var sto_loader = path.join(__dirname, "src/sto_loader.js");

fs.writeFile("dist/snippet.js", snippet);
module.exports = {
    "entry": "./src",
    "output": {
        "path": __dirname + "/dist",
        "filename": filename,
        "publicPath": publicPath
    },
    "module": {
        //sto_loader va permettre de transformer __PLACEHOLDER__ en la valeur de settings.name,
        //nécessaire dans css et html
        "rules": [
            {"test": /\.json$/, use: "json-loader"},
            {"test": /\.css$/, use: [ { loader: "style-loader/useable" }, { loader: "css-loader" }, { loader: sto_loader }]},
            {"test": /\.html$/, use: [ { loader: "html-loader" }, { loader: sto_loader } ]},
            {"test": /\.(png|jpg|gif|jpeg)/, use: "file-loader" },
            {"test": /\.(eot|svg|ttf|woff|woff2)$/,  use: { loader: 'file-loader', options: { name: '[name].[ext]' } } },
            {"test": /\.js$/, use: {loader: 'babel-loader'}}
        ]
    },
    "devServer": {
        "headers": {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
          "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    "plugins": [
        templatePlugin
    ]
};
