"use strict";

const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings"),
  style = require("./main.css"),
  helper_methods = sto.utils.retailerMethod,
  custom = settings.custom,
  format = settings.format,
  creaidformat = settings.creaid,
  border_color = settings.border_color,
  productss = [],
  cta_position = settings.cta_position == '' ? 'bottom' : settings.cta_position,
  cta_target = settings.cta_target == '' ? '_blank' : settings.cta_target,
  classParent = "storetail-id-" + settings.format + settings.creaid,
  trackerfirstbrowse = true,
  option = settings.option;

var titleText = {};

var fontRoboto = document.createElement('link');
fontRoboto.setAttribute('rel', 'stylesheet');
fontRoboto.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto');
var head = document.querySelector('head');
head.appendChild(fontRoboto);

var state = 0;

custom.sort(function(a, b) {
  return a[ctxt_pos] > b[ctxt_pos]
});

// Get all product ids from custom product organization
custom.forEach(function(e) {
  e[ctxt_prodlist].forEach(function(p) {
    productss.push(p);
  })
});

module.exports = {
  init: _init_(),
}


var setCust = settings.custom.map(function(pdt, i) {
  return {
    "id": pdt[3][0],
    "type": "partner-product",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": settings.format,
      "class": "storetail storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
      "position_reference": null,
    }
  }
})

var settings_Format = {
    "ids": productss,
    "type": settings.format,
    "creaId": settings.creaid
  },
  format_setting = [{
    "id": "0000000000",
    "type": "partner-multi-products",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": settings.format,
      "need_products": true,
      "class": "storetail sto-banner storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
      "position_reference": null,
    },
    "products": []
  }];

format_setting[0]["products"] = setCust;

function _init_() {

  sto.load(settings.format, function(tracker) {
    sto.utils.retailerMethod.crawlAPI(settings_Format).promise.then(function(result) {
      sto.utils.retailerMethod.createFormat(tracker, format_setting, result);

      sto.globals.deliveringSettings.ready.toUpdateFormats.promise.then(function() {
        style.use();

        var container = document.querySelector("." + classParent),
          logo = document.createElement("DIV"),
          logo_cta_img = document.createElement("DIV"),
          productContainer = document.createElement("DIV"),
          designLeft = document.createElement("DIV"),
          designRight = document.createElement("DIV"),
          logo2 = document.createElement("DIV"),
          miniProdLeft = document.createElement("DIV"),
          miniProdRight = document.createElement("DIV"),
          prodCenter = document.createElement("DIV"),
          arrowLeft = document.createElement("DIV"),
          arrowRight = document.createElement("DIV");

        logo.className = "sto-logo";
        logo_cta_img.className = "sto-cta-img";
        designLeft.className = "sto-design-left";
        designRight.className = "sto-design-right";
        logo2.className = "sto-logo2";
        productContainer.className = "sto-products-container";
        miniProdLeft.className = "sto-mini-products sto-mini-products-left sto-arrow-left arrows";
        prodCenter.className = "sto-product-center"
        miniProdRight.className = "sto-mini-products sto-mini-products-right sto-arrow-right arrows";
        arrowLeft.className = "sto-arrows-products sto-arrow-left sto-arrows-products-left arrows";
        arrowRight.className = "sto-arrows-products sto-arrow-right sto-arrows-products-right arrows";
        container.querySelector("div").prepend(productContainer)
        container.querySelector("div").prepend(designLeft)
        container.querySelector("div").prepend(logo)
        container.querySelector(".sto-logo").append(logo_cta_img)
        container.querySelector("div").append(logo2)
        container.querySelector("div").append(designRight);


        var articleSize = container.getElementsByClassName("product-card");
        var prodConte = container.querySelector(".sto-products-container");

        prodConte.prepend(miniProdLeft);
        prodConte.append(prodCenter);
        prodConte.append(miniProdRight);
        for (var i = 0; i < articleSize.length; i++) {
          container.querySelector(".sto-product-center").append(articleSize[i]);
          articleSize[i].setAttribute("data-id", articleSize[i].id);
        }
        articleSize[0].style.display = "flex";
        container.querySelector(".sto-product-center").prepend(arrowLeft);
        container.querySelector(".sto-product-center").append(arrowRight);
        var sizeProdConte = prodConte.offsetWidth / 2;
        var miniLeft = document.querySelector(".sto-arrow-left");
        var miniRight = document.querySelector(".sto-arrow-right");
        var miniLeftImg = document.querySelector(".sto-mini-products-left");
        var miniRightImg = document.querySelector(".sto-mini-products-right");

        var buttonsCont = document.createElement('DIV');
        buttonsCont.className = 'sto-buttons-container';

        if (settings.multibrand == true) {
          document.querySelector("." + classParent).setAttribute("multibrand", "");
          option = "";
        }

        for (var i = 0; i < 4; i++) {
          if (document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article')[i]) {
            var currentProdID = document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article')[i].getAttribute('data-id');
            var button = document.createElement('button');
            button.setAttribute('data-id', currentProdID);
            var buttonName;
            for (var j = 0; j < settings.custom.length; j++) {
              for (var k = 0; k < settings.custom[j][3].length; k++) {
                if (settings.custom[j][3][k] == currentProdID) {
                  buttonName = settings.custom[j][1]
                }
              }
            }
            button.setAttribute('data-index', i + 1);
            button.innerHTML = buttonName;
            buttonsCont.appendChild(button);
          }
        }

        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo').appendChild(buttonsCont);

        document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button')[0].className += 'sto-button-selected';

        for (var z = 0; z < document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button').length; z++) {
          document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button')[z].addEventListener('click', function() {
            var selBtnId = this.getAttribute('data-id');
            var selBtnInd = this.getAttribute('data-index');
            for (var i = 0; i < document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button').length; i++) {
              document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button')[i].classList.remove("sto-button-selected");
            }
            for (var i = 0; i < document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article').length; i++) {
              document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article')[i].style.display = 'none';
            }
            this.className += 'sto-button-selected';
            document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article[data-id="' + selBtnId + '"]').style.display = 'flex';
            /*tracker.browse({
              "tl":selBtnId,
              "pi":selBtnId
            })*/
            state = mod(selBtnInd - 1, articleSize.length);
            carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);
            miniPrds(articleSize, miniLeftImg, miniRightImg)
          });
        }

        miniLeftImg.addEventListener('click', function() {
          state = mod(state - 1, articleSize.length);
          carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);
          miniPrds(articleSize, miniLeftImg, miniRightImg)
        });
        miniRightImg.addEventListener('click', function() {
          state = mod(state + 1, articleSize.length);
          carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);
          miniPrds(articleSize, miniLeftImg, miniRightImg)
        });
        arrowLeft.addEventListener('click', function() {
          state = mod(state - 1, articleSize.length);
          carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);
          miniPrds(articleSize, miniLeftImg, miniRightImg)
        });
        arrowRight.addEventListener('click', function() {
          state = mod(state + 1, articleSize.length);
          carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);
          miniPrds(articleSize, miniLeftImg, miniRightImg)
        });

        carrousselUpdate(prodConte, articleSize.length, articleSize, tracker);

        miniPrds(articleSize, miniLeftImg, miniRightImg);


        container.setAttribute('data-count', document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article').length);

        var title = container.getElementsByClassName("title");
        for (var i = 0; i < title.length; i++) {
          titleText[i] = title[i].innerText;
        }

        if (window.matchMedia("(min-width: 812px)").matches) {
          for (var i = 0; i < title.length; i++) {
            if (titleText[i].length > 24) {
              var newTitle = titleText[i].slice(0, 24);
              title[i].innerText = newTitle + "..."
            }
          }
        }

        optionsLink(tracker);
        //debug();

        var trackerIMP = setInterval(function() {
          if (window.getComputedStyle(document.querySelector(".storetail-id-" + settings.format + settings.creaid)).display != "none") {
            tracker.display();
            clearInterval(trackerIMP);
          }
        }, 100);

        window.addEventListener("resize", function() {
          if (window.matchMedia("(min-width: 812px)").matches) {
            for (var i = 0; i < title.length; i++) {
              if (titleText[i].length > 24) {
                var newTitle = titleText[i].slice(0, 24);
                title[i].innerText = newTitle + "..."
              }
            }
          } else {
            for (var i = 0; i < title.length; i++) {
              title[i].innerText = titleText[i]
            }
          }
          if (window.matchMedia("(max-width: 812px)").matches) {
            document.querySelector('.storetail-id-' + settings.format + settings.creaid).setAttribute('data-viewport', 'mobile');
            document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo').style.height = (document.querySelector('.storetail-id-' + settings.format + settings.creaid).offsetWidth * 0.2415459) + 'px';
            document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-products-container').style.paddingTop = (document.querySelector('.storetail-id-' + settings.format + settings.creaid).offsetWidth * 0.2415459) + 'px';

          } else {
            document.querySelector('.storetail-id-' + settings.format + settings.creaid).setAttribute('data-viewport', 'desktop');
            document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo').style.height = "120px";
            document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-products-container').style.paddingTop = "0px";

          }

          // Debugging
          //debug();

        });
        // window.setTimeout(function() {
        //     //window.dispatchEvent(new window.Event("resize"));
        //     var evt = window.document.createEvent('UIEvents');
        //     evt.initUIEvent('resize', true, false, window, 0);
        //     window.dispatchEvent(evt);
        // });




        //TRACKING VIEW
        var inter, isElementInViewport,
        intelement = document.querySelector(".storetail-id-" + settings.format + settings.creaid);
        isElementInViewport = function(el) {
          if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
            el = el[0];
          }
          var rect = el.getBoundingClientRect();
          return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            !(rect.top == rect.bottom || rect.left == rect.right) &&
            !(rect.height == 0 || rect.width == 0) &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
          );
        };
        inter = window.setInterval(function() {
          if (isElementInViewport(intelement)) {
            tracker.view();
            window.clearInterval(inter);
          }
        }, 200);
        /*var el;
        var intervalView = setInterval(function () {
            el = document.querySelector(".storetail-id-" + settings.format + settings.creaid);
            if (el && window.getComputedStyle(el).display != "none") {
                var top = el.offsetTop;
                var left = el.offsetLeft;
                var width = el.offsetWidth;
                var height = el.offsetHeight;
                while (el.offsetParent) {
                    el = el.offsetParent;
                    top += el.offsetTop;
                    left += el.offsetLeft;
                }
                if (top >= window.pageYOffset && left >= window.pageXOffset && (top + height) <= (window.pageYOffset + window.innerHeight) && (left + width) <= (window.pageXOffset + window.innerWidth)) {
                    tracker.view();
                    clearInterval(intervalView);
                }
            }
        }, 100);*/

        container.setAttribute('data-count', document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-product-center article').length);
      }, 500);
    });
  });
}

function mod(x, N) {
  return ((x % N) + N) % N;
}

function carrousselUpdate(cible, count, products, tracker) {
  var left, right;
  left = mod(state - 1, count);
  right = mod(state + 1, count);

  cible.querySelector(".sto-mini-products-left").setAttribute("data-id", products[left].getAttribute("data-id"));
  cible.querySelector(".sto-mini-products-right").setAttribute("data-id", products[right].getAttribute("data-id"));
  cible.querySelector(".sto-arrows-products-left").setAttribute("data-id", products[left].getAttribute("data-id"));
  cible.querySelector(".sto-arrows-products-right").setAttribute("data-id", products[right].getAttribute("data-id"));
  cible.querySelector(".sto-product-center").setAttribute("data-id", products[state].getAttribute("data-id"));
  var id = products[state].getAttribute("data-id");

  if (trackerfirstbrowse == false) {
    tracker.browse({
      "tl": id,
      "pi": id
    });
  }
  trackerfirstbrowse = false;

  for (var i = 0; i < products.length; i++) {
    products[i].style.display = "none";
  }
  products[state].style.display = "flex";

  for (var i = 0; i < document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button').length; i++) {
    document.querySelectorAll('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button')[i].classList.remove("sto-button-selected");
  }
  if (document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button[data-id="' + id + '"]')) {
    document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-buttons-container button[data-id="' + id + '"]').className += 'sto-button-selected';
  }
}

function miniPrds(articleSize, miniLeft, miniRight) {
  for (var i = 0; i < articleSize.length; i++) {
    if (articleSize[i].getAttribute("data-id") === miniLeft.getAttribute("data-id")) {
      miniLeft.style.backgroundImage = "url(" + articleSize[i].querySelector(".image").getAttribute("src") + ")";
    }
    if (articleSize[i].getAttribute("data-id") === miniRight.getAttribute("data-id")) {
      miniRight.style.backgroundImage = "url(" + articleSize[i].querySelector(".image").getAttribute("src") + ")";
    }
  }
}

function optionsLink(tracker) {
  if (option != "none" && option != "") {
    var target = document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-design-right');
    document.querySelector('.storetail-id-' + settings.format + settings.creaid).setAttribute("cta-type", option);
    var cta = document.createElement('span');
    cta.className = 'sto-cta';
    cta.setAttribute('cta-pos', cta_position);
    target.appendChild(cta);

    switch (option) {
      case 'redirection':
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-cta').addEventListener('click', function() {
          tracker.click();
          window.open(settings.cta_redirect, cta_target);
        });
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo .sto-cta-img').addEventListener('click', function() {
          tracker.click();
          window.open(settings.cta_redirect, cta_target);
        });
        break;
      case 'file':
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-cta').addEventListener('click', function() {
          tracker.openPDF();
          var pdf = require("../../img/" + settings.cta_file);
          window.open(pdf, "_blank");
        });
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo .sto-cta-img').addEventListener('click', function() {
          tracker.openPDF();
          var pdf = require("../../img/" + settings.cta_file);
          window.open(pdf, "_blank");
        });
        break;
      case 'video':
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-cta').addEventListener('click', function() {
          tracker.playVideo({
            "tl": "open"
          })
          var videoBackground = document.createElement('div');
          videoBackground.className = 'sto-ecran';
          var videoBox = document.createElement('div');
          videoBox.className = 'sto-video';
          videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_embed_video + '</div>';
          document.querySelector('body').appendChild(videoBackground);
          document.querySelector('body').appendChild(videoBox);
          document.querySelector('.sto-close').addEventListener('click', function() {
            tracker.close({
              "tl": "closeBtn"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
          document.querySelector('.sto-ecran').addEventListener('click', function() {
            tracker.close({
              "tl": "blackScreen"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
        });
        document.querySelector('.storetail-id-' + settings.format + settings.creaid + ' .sto-logo .sto-cta-img').addEventListener('click', function() {
          tracker.playVideo({
            "tl": "open"
          })
          var videoBackground = document.createElement('div');
          videoBackground.className = 'sto-ecran';
          var videoBox = document.createElement('div');
          videoBox.className = 'sto-video';
          videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_embed_video + '</div>';
          document.querySelector('body').appendChild(videoBackground);
          document.querySelector('body').appendChild(videoBox);
          document.querySelector('.sto-close').addEventListener('click', function() {
            tracker.close({
              "tl": "closeBtn"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
          document.querySelector('.sto-ecran').addEventListener('click', function() {
            tracker.close({
              "tl": "blackScreen"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
        });
        break;
      default:
    }
  }

}

/*function debug(){
    if (window.matchMedia("(min-width: 1435px)").matches) {
        console.log('Data size 4');
    }

    if (window.matchMedia("(min-width: 1235px) and (max-width: 1435px)").matches) {
        console.log('Data size 3');
    }

    if (window.matchMedia("(min-width: 1029px) and (max-width: 1234px)").matches) {
        console.log('Data size 2');
    }

    if (window.matchMedia("(min-width: 812px) and (max-width: 1028px)").matches) {
        console.log('Data size 1');
    }

    if (window.matchMedia("(max-width: 812px)").matches) {
        console.log('Mobile');
    }
}*/
