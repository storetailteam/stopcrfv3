var settings = require("./settings.json");
    settings.banner_container_img = settings.banner_container_img === "" ? "none" : "url(./../../img/" + settings.banner_container_img + ")";
    settings.banner_container_img_mob = settings.banner_container_img_mob === "" ? "none" : "url(./../../img/" + settings.banner_container_img_mob + ")";
    settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/" + settings.logo_img + ")";
    settings.logo2_img = settings.logo2_img === "" ? "none" : "url(./../../img/" + settings.logo2_img + ")";
    settings.logo_mob = settings.logo_mob === "" ? "none" : "url(./../../img/" + settings.logo_mob + ")";
    settings.design_left_img = settings.design_left_img === "" ? "none" : "url(./../../img/" + settings.design_left_img + ")";
    settings.arrow_left_img = settings.arrow_left_img === "" ? "none" : "url(./../../img/" + settings.arrow_left_img + ")";
    settings.arrow_left_img_hover = settings.arrow_left_img_hover === "" ? "none" : "url(./../../img/" + settings.arrow_left_img_hover + ")";
    settings.design_right_img = settings.design_right_img === "" ? "none" : "url(./../../img/" + settings.design_right_img + ")";
    settings.design_right_img_big = settings.design_right_img_big === "" ? "none" : "url(./../../img/" + settings.design_right_img_big + ")";
    settings.design_left_img_multi = settings.design_left_img_multi === "" ? "none" : "url(./../../img/" + settings.design_left_img_multi + ")";
    settings.design_right_img_multi = settings.design_right_img_multi === "" ? "none" : "url(./../../img/" + settings.design_right_img_multi + ")";
    settings.miniproduits_img = settings.miniproduits_img === "" ? "none" : "url(./../../img/" + settings.miniproduits_img + ")";
    settings.carrousel_img = settings.carrousel_img === "" ? "none" : "url(./../../img/" + settings.carrousel_img + ")";
    settings.buttons_color = settings.buttons_color === "" ? "transparent" : settings.buttons_color;
    settings.buttons_color_selected = settings.buttons_color_selected === "" ? "transparent" : settings.buttons_color_selected;
    settings.border_color = settings.border_color === "" ? "transparent" : settings.border_color;
    settings.bg_color = settings.bg_color === "" ? "transparent" : settings.bg_color;
    settings.btf_color = settings.btf_color === "" ? "transparent" : settings.btf_color;
    settings.buttons_txt_color = settings.buttons_txt_color === "" ? "transparent" : settings.buttons_txt_color;
    settings.buttons_txt_color_selected = settings.buttons_txt_color_selected === "" ? "transparent" : settings.buttons_txt_color_selected;
    settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
    settings.cta_normal_img = settings.cta_normal_img === "" ? "none" : "url(./../../img/" + settings.cta_normal_img + ")";
    settings.cta_hover_img = settings.cta_hover_img === "" ? "none" : "url(./../../img/" + settings.cta_hover_img + ")";

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.format + settings.creaid)
        .replace(/__ARROW__/g, settings.arrow_left_img)
        .replace(/__ARROW_HOVER__/g, settings.arrow_left_img_hover)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__LOGOIMG__/g, settings.logo_img)
        .replace(/__LOGOIMG2__/g, settings.logo2_img)
        .replace(/__LOGOIMG_MOB__/g, settings.logo_mob)
        .replace(/__BORDER_COLOR__/g, settings.border_color)
        .replace(/__BG_COLOR__/g, settings.bg_color)
        .replace(/__DESIGN_LEFT__/g, settings.design_left_img)
        .replace(/__DESIGN_RIGHT__/g, settings.design_right_img)
        .replace(/__DESIGN_RIGHT_BIG__/g, settings.design_right_img_big)
        .replace(/__DESIGN_LEFT_MULTI__/g, settings.design_left_img_multi)
        .replace(/__DESIGN_RIGHT_MULTI__/g, settings.design_right_img_multi)
        .replace(/__MINIPRODUITS_IMG__/g, settings.miniproduits_img)
        .replace(/__CARROUSEL_IMG__/g, settings.carrousel_img)
        .replace(/__BTFCOLOR__/g, settings.btf_color)
        .replace(/__BUTTONTXT__/g, settings.buttons_txt_color)
        .replace(/__BUTTONTXT_SEL__/g, settings.buttons_txt_color_selected)
        .replace(/__BUTTONCOLOR__/g, settings.buttons_color)
        .replace(/__BUTTONCOLOR_SEL__/g, settings.buttons_color_selected)
        .replace(/__BTF_CONTAINE_IMG__/g, settings.banner_container_img)
        .replace(/__BTF_CONTAINE_IMG_MOB__/g, settings.banner_container_img_mob)
        .replace(/__CTA_IMG__/g, settings.cta_normal_img)
        .replace(/__CTA_IMG_HOVER__/g, settings.cta_hover_img);
    return test;
};